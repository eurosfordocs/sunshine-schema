#!/usr/bin/env python

import click

from src.relational_diagram import generate_relational_diagram


@click.command("Automate work on sunshine europe.")
def cli():
    generate_relational_diagram()


if __name__ == '__main__':
    cli()
