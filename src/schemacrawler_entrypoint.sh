#!/usr/bin/env bash
echo 'Running schema crawler against PostgreSQL'

/opt/schemacrawler/schemacrawler.sh -command=schema \
-infolevel=standard \
-portablenames=true \
-server=postgresql -host=postgres -port=5432 -user=postgres -database=postgres \
-title 'Normalized sunshine act schema' \
-only-matching \
-outputformat=pdf -outputfile=/share/relational_schema.pdf

echo 'Done. Relational diagram has been written in `relational_schema.pdf`'
