from os.path import join as pjoin

NORMALIZED_SCHEMA_DIR = "normalized_schema"
NORMALIZED_TABLE_SCHEMA_DIR = pjoin(NORMALIZED_SCHEMA_DIR, "tableschema")
