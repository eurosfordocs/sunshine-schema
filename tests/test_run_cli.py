from click.testing import CliRunner

from main import cli

runner = CliRunner()


def test_run_cli_default():
    runner.invoke(cli, catch_exceptions=False)
