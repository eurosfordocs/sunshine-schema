import logging
import os

import pytest
from tableschema import Schema

from src.utils import get_all_schema_path


@pytest.mark.parametrize('schema_path', get_all_schema_path())
def test_tableschema_is_valid(schema_path):
    schema = Schema(schema_path)
    if not schema.valid:
        for error in schema.errors:
            logging.info("Error in schema at path '{}'".format(schema_path))
            logging.error(error)
        assert schema.valid


@pytest.mark.parametrize('schema_path', get_all_schema_path())
def test_tableschema_name(schema_path):
    filename = os.path.basename(schema_path)[:-5]
    schema = Schema(schema_path)
    name = schema.descriptor["name"]
    assert name == filename


def test_get_all_schema_path_return_schemas():
    number_of_schemas = len(list(get_all_schema_path()))
    assert number_of_schemas >= 2


@pytest.mark.parametrize('schema_path', get_all_schema_path())
def test_tableschema_contains_all_fields(schema_path):
    descriptor = Schema(schema_path).descriptor

    assert sorted(list(set(descriptor.keys()) - set(["foreignKeys", "primaryKey"]))) == \
           sorted(["fields", "missingValues", "name", ])

    for field in descriptor['fields']:
        assert not set(['name', 'title', 'type', 'description']) - set(field.keys())
